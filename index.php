<!DOCTYPE html>
<html>
<head>
	<title>Math Utilities</title>
	<meta name=viewport content="width=device-width,initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.4.3/css/foundation.min.css" integrity="sha256-GSio8qamaXapM8Fq9JYdGNTvk/dgs+cMLgPeevOYEx0=" crossorigin="anonymous" />
</head>
<body>
    <div class="callout primary">
        <h2 class="text-center">Math Utilities</h2>
    </div>

    <div class="grid-container">
        <div class="grid-x align-center grid-margin-x">
            <div class="cell small-12">
                <h4>Available Utilities</h4>
                <ul>
                    <!--<li><a href="utilities/geogebra-preview.php">GeoGebra - File Preview</a></li>-->
                    <li><a href="utilities/geogebra.php">GeoGebra - Base64 Extraction</a></li>
                    <li><a href="utilities/hint.php">Hint Content Composer</a></li>
                </ul>

                <h4>SQL Queries</h4>
                <ul>
                    <li><a href="sql/?preview=cleanup_deleted_content.sql" >CDL - Cleanup Deleted Content</a></li>
                    <li><a href="sql/?preview=delete_all_answers.sql" >CDL - Delete All Answer</a></li>
                </ul>
            </div>
        </div>
    </div>
</body>
</html>