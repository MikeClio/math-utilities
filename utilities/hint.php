<!DOCTYPE html>
<html>
<head>
	<title>Hint</title>
	<meta name=viewport content="width=device-width,initial-scale=1">
    <script src="https://cdn.geogebra.org/apps/deployggb.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.4.3/css/foundation.min.css" integrity="sha256-GSio8qamaXapM8Fq9JYdGNTvk/dgs+cMLgPeevOYEx0=" crossorigin="anonymous" />
</head>
<body>
    <script>

      document.addEventListener('DOMContentLoaded', function() {
        generateJson()
      });

      let json = {
        "title": "",
        "body": "",
        "copyOf": null,
        "height": 0,
        "language": null,
        "ownerAccount": null,
        "ownerUser": null,
        "parent": null,
        "sortIndex": 1,
        "width": 0,
        "image": null
      };

      function selectImage() {
        let file = document.getElementById('hint-image').files[0];
        let reader  = new FileReader();

        reader.addEventListener("load", function () {
          json.image = reader.result;
          generateJson();
        }, false);

        if (file) {
          reader.readAsDataURL(file);
        }
      }

      function generateJson() {
        json.title = document.getElementById('hint_title').value;
        json.body = document.getElementById('hint_body').value;
        json.parent = document.getElementById('hint_parent').value;
        json.sortIndex = document.getElementById('hint_index').value;

        document.querySelector('#json').value = JSON.stringify(json, null, 4);
      }
    </script>

    <div class="callout primary">
        <h2 class="text-center">Hint content composer</h2>
    </div>

    <div class="grid-container">
        <div class="grid-x align-center grid-margin-x">
            <div class="cell medium-8 large-6">
                <div class="grid-container full">
                    <div class="grid-x grid-margin-x">
                        <div class="medium-12 cell">
                            <label>
                                Title:
                                <input type="text" id="hint_title" onchange="generateJson()" onkeyup="generateJson()">
                            </label>
                        </div>
                        <div class="medium-12 cell">
                            <label>
                                Hint Text:
                                <textarea rows="2" id="hint_body" onchange="generateJson()" onkeyup="generateJson()"></textarea>
                            </label>
                        </div>
                        <div class="medium-12 cell">
                            <label>
                                Hint Image:
                                <input type="file" id="hint-image" accept="image/*" onchange="selectImage()">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cell medium-8 large-6">
                <div class="grid-container full">
                    <div class="grid-x grid-margin-x">
                        <div class="medium-12 cell">
                            <label>
                                Parent ID:
                                <input type="text" id="hint_parent" onchange="generateJson()" onkeyup="generateJson()">
                            </label>
                        </div>
                        <div class="medium-12 cell">
                            <label>
                                Sort Index (Order of hint related to other hints)
                                <input type="number" id="hint_index" onchange="generateJson()" onkeyup="generateJson()">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="grid-container">
        <div class="grid-x align-center">
            <div class="cell medium-8 large-12">
                <h3>JSON for POST:</h3>
                <div>
                    <button type="button" class="button secondary small" id="jsonGenerate" onclick="generateJson()">Update JSON</button>
                </div>
                <textarea id="json" rows="20"></textarea>
            </div>
        </div>
    </div>
</body>
</html>