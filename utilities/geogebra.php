<!DOCTYPE html>
<html>
<head>
	<title>Geogebra - Base64 extraction</title>
	<meta name=viewport content="width=device-width,initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.4.3/css/foundation.min.css" integrity="sha256-GSio8qamaXapM8Fq9JYdGNTvk/dgs+cMLgPeevOYEx0=" crossorigin="anonymous" />
    <script src="https://cdn.geogebra.org/apps/deployggb.js"></script>
    <style>
        #json pre {
            background: #eee;
            padding: 10px;
            word-break: break-all;
            white-space: pre-wrap;
        }
        .ggb-container-wrapper {
            border: 1px dotted red;
            width: 555px;
        }
        .ggb-container {
            width: 555px;
            height: 555px;
        }
    </style>
</head>
<body>
    <script>
      document.addEventListener('DOMContentLoaded', function() {
        document.getElementById('ggb-base64').value = '';
        document.getElementById('ggb-file').value = '';
        document.getElementById('title').value = '';
      });

      let json = <?php echo file_get_contents('payload.json'); ?>;
      let dataURL = '';

      function selectGeogebra() {
        let file = document.getElementById('ggb-file').files[0];
        let reader  = new FileReader();

        reader.addEventListener("load", function () {
          dataURL = reader.result;
          previewGeogebra(dataURL);
        }, false);

        if (file) {
          reader.readAsDataURL(file);
        }
      }

      function previewGeogebra(url) {
        let base64 = url.replace(/^data:.*\/.*;base64,/, '');
        let appNameSelector = document.getElementById('ggb-appName');
        let appName = appNameSelector.options[appNameSelector.selectedIndex].value;
        let height = document.getElementById("ggb-height").value;
        let ggbContainer = document.getElementsByClassName("ggb-container");
        let showMenuBar = document.getElementById("ggb-showMenuBar").checked;
        let showToolBar = document.getElementById("ggb-showToolBar").checked;
        let showToolBarHelp = document.getElementById("ggb-showToolBar").checked;
        let showAlgebraInput = document.getElementById("ggb-showToolBar").checked;
        let algebraInputPositionSelector = document.getElementById("ggb-algebraInputPosition");
        let algebraInputPosition = algebraInputPositionSelector.options[algebraInputPositionSelector.selectedIndex].value;
        let allowStyleBar = document.getElementById("ggb-allowStyleBar").checked;
        let showResetIcon = document.getElementById("ggb-showResetIcon").checked;
        let enableRightClick = document.getElementById("ggb-enableRightClick").checked;
        let enableLabelDrags = document.getElementById("ggb-enableLabelDrags").checked;
        let enableShiftDragZoom = document.getElementById("ggb-enableShiftDragZoom").checked;
        let playButton = document.getElementById("ggb-playButton").checked;
        let perspectiveSelector = document.getElementById('ggb-perspective');
        let perspective = perspectiveSelector.options[perspectiveSelector.selectedIndex].value;
        let allowUpscale = document.getElementById("ggb-allowUpscale").checked;

        height = parseInt(height);
        height = isNaN(height) ? 0 : height;

        for (var i = 0; i < ggbContainer.length; i++) {
            ggbContainer[i].style.height = height + "px";
        }

        const parameters =
          {
            "id": "ggbPreview",
            "appName": appName,
            "ggbBase64": base64,
            "width": 555,//width
            "height": height,//height
            "borderColor": '#888888',//border
            "showMenuBar": showMenuBar,//smb
            "showToolBar": showToolBar,//stb
            "showToolBarHelp": showToolBarHelp,//stbh
            "showAlgebraInput" : showAlgebraInput,
            "algebraInputPosition": algebraInputPosition,//ai
            "allowStyleBar": allowStyleBar,//asb
            "showResetIcon": showResetIcon,//sri
            "enableRightClick": enableRightClick,//rc
            "enableLabelDrags": enableLabelDrags,//ld
            "enableShiftDragZoom": enableShiftDragZoom,//sdz
            "useBrowserForJS": false,
            "playButton": playButton,//ctl,
            "perspective": perspective,
            "scaleContainerClass": "ggb-container",
            "allowUpscale" : allowUpscale
          };

        document.getElementById('ggb-base64').value = base64;
        json.body = base64;
        json.appName = appName;
        json.width = 555;
        json.height = height;
        json.showMenuBar = showMenuBar;
        json.showToolBar = showToolBar;
        json.showToolBarHep = showToolBarHelp;
        json.showAlgebraInput = showAlgebraInput;
        json.algebraInputPosition = algebraInputPosition;
        json.allowStyleBar = allowStyleBar;
        json.showResetIcon = showResetIcon;
        json.enableRightClick = enableRightClick;
        json.enableLabelDrags = enableLabelDrags;
        json.enableShiftDragZoom = enableShiftDragZoom;
        json.playButton = playButton;
        json.perspective = perspective;
        json.allowUpscale = allowUpscale;

        generateJson();

        var ggbApp = new GGBApplet("5.0", parameters);
        ggbApp.setHTML5Codebase('../assets/GeoGebra/HTML5/5.0/web3d/');
        ggbApp.inject('ggb-preview');
      }

      function generateJson() {
        json.title = document.getElementById('title').value;
        document.querySelector('#json pre').innerHTML = JSON.stringify(json, null, 4);
      }

      function generateBase64() {
        let base64 = ggbPreview.getBase64();
        document.getElementById('ggb-base64').value = base64;
        json.body = base64;
        generateJson();
      }
    </script>



    <div class="grid-container">
        <div class="grid-x grid-margin-x align-center">
            <div class="cell small-6 medium-6">
                <div>
                    <h2>Preview GeoGebra file</h2>
                    <input type="file" id="ggb-file" accept=".ggb" onchange="selectGeogebra()">
                </div>
                <h3>Preview:</h3>
                <div class="ggb-container-wrapper">
                    <div class="ggb-container">
                        <div id="ggb-preview"></div>
                    </div>
                </div>
            </div>
            <div class="cell small-6 medium-6">
                <h2>Controls</h2>
                <label>
                    App Name
                    <select id="ggb-appName" onchange="previewGeogebra(dataURL)">
                        <option value="">None</option>
                        <option value="classic">classic</option>
                        <option value="graphing">GeoGebra Graphing Calculator</option>
                        <option value="geometry">GeoGebra Geometry</option>
                        <option value="3d">GeoGebra 3D Graphing Calculator</option>
                    </select>
                </label>
                <label>
                    Height
                    <input type="number" value="555" id="ggb-height" min="0" onkeyup="previewGeogebra(dataURL)" onchange="previewGeogebra(dataURL)">
                </label>
                <div class="grid-x grid-margin-x">
                    <div class="cell small-6 medium-6">
                        <label>
                            Show Menu Bar
                            <div class="switch">
                                <input class="switch-input" id="ggb-showMenuBar" type="checkbox" onchange="previewGeogebra(dataURL)">
                                <label class="switch-paddle" for="ggb-showMenuBar">
                                    <span class="show-for-sr">Show Menu Bar</span>
                                </label>
                            </div>
                        </label>
                        <label>
                            Show Tool Bar
                            <div class="switch">
                                <input class="switch-input" id="ggb-showToolBar" type="checkbox" onchange="previewGeogebra(dataURL)">
                                <label class="switch-paddle" for="ggb-showToolBar">
                                    <span class="show-for-sr">Show Tool Bar</span>
                                </label>
                            </div>
                        </label>
                        <label>
                            Show Tool Bar Help
                            <div class="switch">
                                <input class="switch-input" id="ggb-showToolBarHelp" type="checkbox" onchange="previewGeogebra(dataURL)">
                                <label class="switch-paddle" for="ggb-showToolBarHelp">
                                    <span class="show-for-sr">Show Tool Bar Help</span>
                                </label>
                            </div>
                        </label>
                        <label>
                            Show Algebra Input
                            <div class="switch">
                                <input class="switch-input" id="ggb-showAlgebraInput" type="checkbox" onchange="previewGeogebra(dataURL)">
                                <label class="switch-paddle" for="ggb-showAlgebraInput">
                                    <span class="show-for-sr">Show Algebra Input</span>
                                </label>
                            </div>
                        </label>
                        <label>
                            Algebra Input Position
                            <select id="ggb-algebraInputPosition" onchange="previewGeogebra(dataURL)">
                                <option value="">Empty</option>
                                <option value="algebra">Algebra</option>
                                <option value="top">Top</option>
                                <option value="bottom">Bottom</option>
                            </select>
                        </label>
                        <label>
                            Allow Style Bar
                            <div class="switch">
                                <input class="switch-input" id="ggb-allowStyleBar" type="checkbox" onchange="previewGeogebra(dataURL)">
                                <label class="switch-paddle" for="ggb-allowStyleBar">
                                    <span class="show-for-sr">Allow Style Bar</span>
                                </label>
                            </div>
                        </label>
                    </div>
                    <div class="cell small-6 medium-6">
                        <label>
                            Show Reset Icon
                            <div class="switch">
                                <input class="switch-input" id="ggb-showResetIcon" type="checkbox" onchange="previewGeogebra(dataURL)">
                                <label class="switch-paddle" for="ggb-showResetIcon">
                                    <span class="show-for-sr">SShow Reset Icon</span>
                                </label>
                            </div>
                        </label>
                        <label>
                            Enable Right Click
                            <div class="switch">
                                <input class="switch-input" id="ggb-enableRightClick" type="checkbox" onchange="previewGeogebra(dataURL)">
                                <label class="switch-paddle" for="ggb-enableRightClick">
                                    <span class="show-for-sr">Enable Right Click</span>
                                </label>
                            </div>
                        </label>
                        <label>
                            Enable Label Drags
                            <div class="switch">
                                <input class="switch-input" id="ggb-enableLabelDrags" type="checkbox" onchange="previewGeogebra(dataURL)">
                                <label class="switch-paddle" for="ggb-enableLabelDrags">
                                    <span class="show-for-sr">Enable Label Drags</span>
                                </label>
                            </div>
                        </label>
                        <label>
                            Enable Shift Drag Zoom
                            <div class="switch">
                                <input class="switch-input" id="ggb-enableShiftDragZoom" type="checkbox" onchange="previewGeogebra(dataURL)">
                                <label class="switch-paddle" for="ggb-enableShiftDragZoom">
                                    <span class="show-for-sr">Enable Shift Drag Zoom</span>
                                </label>
                            </div>
                        </label>
                        <label>
                            Perspective
                            <select id="ggb-perspective" onchange="previewGeogebra(dataURL)">
                                <option value="">Default</option>
                                <option value="1">Algebra and Graphics</option>
                                <option value="2">Geometry</option>
                                <option value="3">Spreadsheet</option>
                                <option value="4">CAS</option>
                                <option value="5">3D Graphics</option>
                                <option value="6">Probability</option>
                            </select>
                        </label>
                        <label>
                            Play Button
                            <div class="switch">
                                <input class="switch-input" id="ggb-playButton" type="checkbox" onchange="previewGeogebra(dataURL)">
                                <label class="switch-paddle" for="ggb-playButton">
                                    <span class="show-for-sr">Play Button</span>
                                </label>
                            </div>
                        </label>
                        <label>
                            Allow Upscale
                            <div class="switch">
                                <input class="switch-input" id="ggb-allowUpscale" type="checkbox" onchange="previewGeogebra(dataURL)">
                                <label class="switch-paddle" for="ggb-allowUpscale">
                                    <span class="show-for-sr">Allow Upscale</span>
                                </label>
                            </div>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-x grid-margin-x align-center">
            <div class="cell small-6 medium-6">
                <h3>Base64:</h3>
                <textarea id="ggb-base64" rows="7" cols="60"></textarea>
                <button class="button" id="jsonGenerate" onclick="generateBase64()">Update</button>
            </div>
            <div class="cell small-6 medium-6">
                <h3>JSON for POST:</h3>
                <div>
                    <label>Title: <input type="text" id="title"></label>
                    <button class="button" id="jsonGenerate" onclick="generateJson()">Update</button>
                </div>
            </div>
            <div class="small-12 medium-12">
                <div id="json"><pre></pre></div>
            </div>
        </div>
    </div>

    <div>

    </div>
</body>
</html>