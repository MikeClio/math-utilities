<?php
require('vendor/autoload.php');
require_once('classes/Tester.php');
include('configuration/config.php');

$_GET['debug'] = false;

if (!isset($configuration)) {
	echo 'Configuration missing! Create config.php in /configuration!';
}

if (empty($argv) || count($argv) < 2) {
	echo 'Not enough command line arguments!';
} else if ($argv[1] == 'help') {
	echo 'Arguments:' . PHP_EOL;
	echo '1: number of students to generate' . PHP_EOL;
	echo '2: number of teachers to generate' . PHP_EOL;
	echo '3: number of editors to generate' . PHP_EOL;
} else {
	$tester = new \Math\StressTest\Tester($argv, $configuration, true);
	$tester->generateTokens();
	//$tester->listUsers();
	$tester->run();
}