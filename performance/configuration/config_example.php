<?php

$configuration = [
	'baseUrl' => 'mike.cliodev.dk',
	'students' => [
		'27ca0bb1-5673-4099-99c2-e08018f1b161', // Harry
		'331be65c-2624-429e-9a8c-ac458e97e8fb' // Ginny
	],
	'teachers' => [
		'4e2cbf5e-f189-4bb3-9eed-d7f5012b6919' // Albus
	],
	'editors' => [
		'075f7b7b-a1c4-4a28-b77c-24747f3316f0' // Myclio
	]
];