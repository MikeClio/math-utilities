<?php
namespace Math\StressTest;

require_once('Client.php');

use AsyncPHP\Doorman\Manager\ProcessManager;
use AsyncPHP\Doorman\Task\ProcessCallbackTask;

class Tester {

	const ARG_STUDENTS 	= 1;
	const ARG_TEACHERS 	= 2;
	const ARG_EDITORS 	= 3;

	private $verbose = false;

	protected $students = [];
	protected $teachers = [];
	protected $editors = [];

	protected $config = [];

	public function __construct($argv, $config, $verbose = false) {
		$this->config = $config;
		$this->verbose = $verbose;
		$this->parseArguments($argv);
	}

	private function parseArguments($argv) {
		if (!empty($argv[self::ARG_STUDENTS])) {
			$this->createStudents($argv[self::ARG_STUDENTS]);
		}
		if (!empty($argv[self::ARG_TEACHERS])) {
			$this->createTeachers($argv[self::ARG_TEACHERS]);
		}
		if (!empty($argv[self::ARG_EDITORS])) {
			$this->createEditors($argv[self::ARG_EDITORS]);
		}
	}

	private function createStudents($numberOfStudents) {
		$this->createUsers($numberOfStudents, 'students');
	}

	private function createTeachers($numberOfTeachers) {
		$this->createUsers($numberOfTeachers, 'teachers');
	}

	private function createEditors($numberOfEditors) {
		$this->createUsers($numberOfEditors, 'editors');
	}

	private function createUsers($numberOfUsers, $userType) {
		for($i = 0; $i < $numberOfUsers; $i++) {
			$identifier = next($this->config[$userType]);
			if (!$identifier) {
				$identifier = reset($this->config[$userType]);
			}
			$this->{$userType}[] = [
				'type' => $userType,
				'identifier' => $identifier,
				'client' => new Client(),
				'access_token' => '',
			];
			if ($this->verbose) {
				echo 'Creating: ' . $identifier . ' in ' . $userType . PHP_EOL;
			}
		}
	}

	public function listUsers() {
		var_dump($this->students);
		var_dump($this->teachers);
		var_dump($this->editors);
	}

	public function generateTokens() {
		foreach($this->students as $key => $student) {
			$this->students[$key]['access_token'] = $student['client']->getAccessToken($student['identifier']);
			if ($this->verbose) {
				echo 'Assigning token: ' . $this->students[$key]['access_token'] . ' to ' . $student['identifier'] . PHP_EOL;
			}
		}
		foreach($this->teachers as $key => $teacher) {
			$this->teachers[$key]['access_token'] = $teacher['client']->getAccessToken($teacher['identifier']);
			if ($this->verbose) {
				echo 'Assigning token: ' . $this->teachers[$key]['access_token'] . ' to ' . $teacher['identifier'] . PHP_EOL;
			}
		}
		foreach($this->editors as $key => $editor) {
			$this->editors[$key]['access_token'] = $editor['client']->getAccessToken($editor['identifier']);
			if ($this->verbose) {
				echo 'Assigning token: ' . $this->editors[$key]['access_token'] . ' to ' . $editor['identifier'] . PHP_EOL;
			}
		}
	}

	public function run() {
		if ($this->verbose) {
			echo 'Starting run with ' . count($this->students) .
				' students | ' . count($this->teachers) .
				' teachers | ' . count($this->editors) .
				' editors' . PHP_EOL;
		}

		$this->deleteLogFiles();
		$this->deleteExistingQueue();
		$this->createQueue();

		$manager = new ProcessManager();
		$manager->setLogPath($this->config['logPath']);

		$totalUsers = count($this->students) + count($this->teachers) + count($this->editors);
		if ($this->verbose) {
			echo 'Processing ' . $totalUsers . ' users' . PHP_EOL;
		}

		for ($i = 0; $i < $totalUsers; $i++) {
			$task = $this->createTask();
			$manager->addTask($task);
		}

		while ($manager->tick()) {
			usleep(250);
		}
	}

	private function createTask() {
		return new ProcessCallbackTask(function () {
			$files = scandir('queue');
			$processId = getmypid();
			if ($files && isset($files[2])) {
				$firstFile = $files[2];
				$json = file_get_contents('queue/' . $firstFile);
				if ($json && $row = json_decode($json, true)) {
					unlink('queue/'.$firstFile);

					$configuration = [];
					require_once(__DIR__ . '/Task.php');
					require_once(__DIR__ . '/../configuration/config.php');
					$task = new Task($row, $processId, $configuration);
					$task->run();
				}
			} else {
				echo $processId . ': No files remaining!' . PHP_EOL;
			}
		});
	}

	private function deleteLogFiles() {
		if ($this->verbose) {
			echo 'Deleting existing log files' . PHP_EOL;
		}
		$files = glob('logs/*');
		foreach ($files as $file) {
			if (is_file($file)) {
				unlink($file);
			}
		}
	}

	private function deleteExistingQueue() {
		if ($this->verbose) {
			echo 'Deleting existing queue files' . PHP_EOL;
		}
		$files = glob('queue/*');
		foreach ($files as $file) {
			if (is_file($file)) {
				unlink($file);
			}
		}
	}

	private function createQueue() {
		$users = array_merge($this->students, $this->teachers, $this->editors);
		shuffle($users);

		foreach ($users as $key => $user) {
			$this->createQueueFile($user, $key);
		}
	}

	private function createQueueFile($row, $key) {
		unset($row['client']);
		$json = json_encode($row);
		$filename = 'queue/' . $key . '.json';
		file_put_contents($filename, $json);
		if ($this->verbose) {
			echo 'Created queue file: ' . $filename . PHP_EOL;
		}
	}
}