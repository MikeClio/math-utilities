<?php
namespace Math\StressTest;

require_once(__DIR__ . '/Client.php');
require_once(__DIR__ . '/RestApiClient.php');

class Task {
	protected $user = [];
	protected $processId = null;
	protected $sdkClient = null;
	protected $restApiClient = null;
	protected $logFile = null;
	protected $startTime = null;
	
	public function __construct($user, $processId, $configuration) {
		$this->user = $user;
		$this->processId = $processId;

		$this->sdkClient = new Client();
		$this->restApiClient = new RestApiClient(
			$this->user['access_token'],
			$configuration['baseUrl'] . '/myclio/apigw/v1/');
	}

	public function run() {
		// create log file
		$this->createLogFile($this->user['identifier'] . "_" . $this->user['type'] . "_" . $this->processId . "_" . time());

		// this is needed to stop the SDK client from spamming the log with warnings
		$_GET['debug'] = false;

		// get start time
		$this->startTime = $this->getTime();

		$this->writeToLog($this->processId . ': Processing scenario for ' . $this->user['identifier'] . PHP_EOL);
		$this->writeToLog($this->processId . ': User is of type: ' . $this->user['type'] . PHP_EOL);
		if ($this->user['type'] == 'students') {
			$this->writeToLog($this->processId . ': Retrieving all content for student and creating answers:' . PHP_EOL);
			$params['access_token'] = $this->user['access_token'];

			$parameters = [
				'answer' => ''
			];
			$content = $this->restApiClient->request(
				'endpoints',
				$parameters
			);

			$contentIds = [];
			foreach ($content as $item) {
				if (!isset($item['id'])) {
					print_r($item );
					echo PHP_EOL;
					print_r($this->restApiClient->getHeaders());
					echo PHP_EOL;
				} else {
					$contentIds[] = $item['id'];
				}
			}

			$parameters = [
				'id' => implode(",", $contentIds),
				'withAnswers' => $this->user['identifier'],
				'validate' => 1,
				'createAnswerContentIfEmpty' => 1
			];

			$contentWithAnswers = $this->restApiClient->request(
				'endpoints',
				$parameters
			);
			$this->writeToLog($this->processId . ': Content with answers: ' . count($contentWithAnswers) . PHP_EOL);
		} else if ($this->user['type'] == 'teachers') {
			$this->writeToLog($this->processId . ': Retrieving all content for all students:' . PHP_EOL);
			$params['access_token'] = $this->user['access_token'];
			$params['answer'] = 'student';
			$content = $this->sdkClient->getInteractiveContent($params);
			$contentIds = [];
			$studentIds = [];

			foreach ($content as $item) {
				$studentIds[] = $item['ownerUser'];
				$contentIds[] = $item['originalContent'];
			}
			$this->writeToLog($this->processId . ': Has retrieved ' . count($contentIds) . ' content for ' . count($studentIds) . ' students' . PHP_EOL);

			$contentWithAnswers = [];
			foreach($studentIds as $studentId) {
				$contentWithAnswers[] = $this->sdkClient->getInteractiveContentAnswers($contentIds, $studentId);
			}
			$this->writeToLog($this->processId . ': Has retrieved ' . count($contentWithAnswers) . ' for all students' . PHP_EOL);
		} else if ($this->user['type'] == 'editors') {
			// creating a Geogebra
			$data = file_get_contents('data/create_payload.json');
			$data = json_decode($data, true);
			$data['ownerUser'] = $this->user['identifier'];
			$data['title'] = 'Stress Test Geogebra ' . $this->processId;
			$this->writeToLog($this->processId . ': Creating a new geogebra element with name: ' . $data['title'] . PHP_EOL);
			$response = $this->restApiClient->request('geogebra', $data, 'POST');
			$this->writeToLog($this->processId . ': Create Response: ' . PHP_EOL . print_r($response, true) . PHP_EOL);

			// updating the Geogebra
			$this->writeToLog($this->processId . ': Updating a geogebra element: ' . $response['id'] . PHP_EOL);
			$data['capturingThreshold'] = 1;
			$data['author'] = 'Updated Author';
			$data['title'] .= ' UPDATED';
			unset($data['__meta']);
			unset($data['__type']);
			$response = $this->restApiClient->request('geogebra/' . $response['id'], $data, 'PATCH');
			$this->writeToLog($this->processId . ': Update Response: ' . PHP_EOL . print_r($response, true) . PHP_EOL);

			// deleting the geogebra
			$this->writeToLog($this->processId . ': Deleting a geogebra element: ' . $response['id'] . PHP_EOL);
			$response = $this->restApiClient->request('geogebra/' . $response['id'], $data, 'DELETE');
			$this->writeToLog($this->processId . ': Delete Response: ' . PHP_EOL . print_r($response, true) . PHP_EOL);
		}
		$this->writeToLog($this->processId . ': Scenario complete for user ' . $this->user['identifier'] . PHP_EOL);
	}

	protected function createLogFile($fileName) {
		$filename = 'logs/' . $fileName . '.txt';
		$content = 'StressTest Scenario Log - ' . $filename . PHP_EOL;
		file_put_contents($filename, $content);
		$this->logFile = $filename;
	}

	protected function writeToLog($line) {
		$currentTime = $this->getTime() - $this->startTime;
		$line = $line . "(" . $currentTime . " sec) ";
		file_put_contents($this->logFile, $line, FILE_APPEND);
	}

	private function getTime() {
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
}