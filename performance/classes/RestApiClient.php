<?php
namespace Math\StressTest;

class RestApiClient {

	/**
	 *  request timeout
	 */
	const REQUEST_TIMEOUT = 60; // in seconds

	/**
	 * @var string $baseUrl The base url for the REST API, prefixed to all calls
	 */
	protected $baseUrl = null;
	/**
	 * @var string $authUrl Identity authentication endpoint for the REST API
	 */
	protected $authEndpoint = null;
	/**
	 * @var array $credentials Credentials to be attached to the authentication URL
	 */
	protected $credentials = [];
	/**
	 * @var array $httpHeader Content type and accepted header for all calls
	 */
	protected $httpHeader = [
		'Content-Type: application/json'
	];
	/**
	 * @var string $accessToken Access token for calls, attached if it exists (always as a GET param)
	 */
	protected $accessToken = null;
	/**
	 * @var integer $tokenExpires Time until the access token expires (from when it was requested)
	 */
	public $tokenExpires = null;
	/**
	 * @var integer $tokenRequested The unix timestamp for when the token was requested
	 */
	public $tokenRequested = null;
	/**
	 * @var mixed $lastReponse The last response to come from a REST API call
	 */
	public $lastResponse = null;

	public function __construct($token = null, $baseUrl = null) {
		if ($token) {
			$this->setAccessToken($token);
		}

		if ($baseUrl) {
			$this->setBaseUrl($baseUrl);
		}
	}

	/**
	 * Triggers the identification procedure in the REST API, setting the returned access_token (if one is returned)
	 * and the expiry time.
	 * @return bool
	 * @throws \Exception
	 */
	public function identify() {
		$this->tokenRequested = time();
		$response = $this->request($this->authEndpoint, $this->credentials);
		if(isset($response['access_token']) && $response['access_token']) {
			$this->accessToken = $response['access_token'];
			if(isset($response['expires_in'])) {
				$this->tokenExpires = $response['expires_in'];
			}
			$response['created'] = time();
			return true;
		}

		return false;
	}

	public function setAccessToken($token) {
		$this->accessToken = $token;
		$this->httpHeader[] = 'Authorization: Bearer ' . $this->accessToken;
	}

	public function setBaseUrl($url) {
		$this->baseUrl = $url;
	}

	public function appendToHeader($header) {
		$this->httpHeader[] = $header;
	}

	public function getHeaders() {
		return $this->httpHeader;
	}

	/**
	 * Fires off the actual request to the REST api
	 *
	 * @param string $endPoint The location of the endpoint to trigger
	 * @param array $parameters All parameters to be sent along (these are automatically json_encoded for POST)
	 * @param string $requestType The requestType, either GET or POST
	 * @param null $referer
	 * @param array $additionalQueryParams If supplied, will append additional query params to POST calls
	 * @return mixed
	 * @throws \Exception
	 */
	public function request($endPoint, $parameters, $requestType = 'GET', $referer = null, $additionalQueryParams = []) {
		if ($requestType == 'GET') {
			$parameters = implode("&", $this->implodeParametersForGET($parameters));
			$ch = curl_init($this->baseUrl . $endPoint . "?" . $parameters);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $this->httpHeader);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::REQUEST_TIMEOUT);
			curl_setopt($ch, CURLOPT_TIMEOUT, self::REQUEST_TIMEOUT);
		} else if ($requestType == 'POST' || $requestType == 'PATCH' || $requestType == 'DELETE') {
			$url = $this->baseUrl . $endPoint;
			if (!empty($additionalQueryParams)) {
				foreach ($additionalQueryParams as $key => $value) {
					$url .= "&" . $key . "=" . $value;
				}
			}
			$ch = curl_init($url);
			$requestBody = json_encode($parameters);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $this->httpHeader);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::REQUEST_TIMEOUT);
			curl_setopt($ch, CURLOPT_TIMEOUT, self::REQUEST_TIMEOUT);

			if ($requestType != 'POST') {
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $requestType);
			}
		} else {
			// invalid requestType, throw exception
			throw new \Exception("REST API: Invalid requestType (".$requestType.")");
		}

		if ($referer) {
			curl_setopt($ch, CURLOPT_REFERER, $referer);
		}

		if (isset($ch) && $ch) {
			$response = curl_exec($ch);
			curl_close($ch);

			if (in_array('Content-Type: application/json', $this->httpHeader)) {
				$response = json_decode($response, true);
			}

			$this->lastResponse = $response;
			return $response;
		} else {
			// curl init failed, throw exception
			throw new \Exception("REST API: Curl init failed.");
		}
	}

	public function hasIdentityExpired() {
		if ($this->accessToken && $this->tokenRequested) {
			if($this->tokenExpires || (time() - $this->tokenRequested) > $this->tokenExpires) {
				return true;
			}
		}
		return false;
	}

	protected function implodeParametersForGET($parameters) {
		foreach ($parameters as $key => $value) {
			$parameters[$key] = $key . "=" . $value;
		}

		return $parameters;
	}
}
