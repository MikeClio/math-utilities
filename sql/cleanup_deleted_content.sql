CREATE TEMPORARY TABLE soft_deleted_content_ids
SELECT content.id AS id
FROM clioonline_content_domain_model_content AS content
LEFT JOIN clioonline_content_domain_model_content AS parent
	ON parent.id = content.originalcontent
WHERE (content.deletedat IS NOT NULL OR parent.deletedat IS NOT NULL);

DELETE conf
FROM clioonline_content_domain_model_contentconfiguration AS conf
WHERE conf.content IN (SELECT id FROM soft_deleted_content_ids);

DELETE prop
FROM clioonline_content_domain_model_contentcontenttypeproperty AS prop
WHERE prop.content IN (SELECT id FROM soft_deleted_content_ids);

DELETE dimjoin
FROM clioonline_content_domain_model_content_dimensions_join AS dimjoin
WHERE dimjoin.content_content IN (SELECT id FROM soft_deleted_content_ids);

DELETE content
FROM clioonline_content_domain_model_content AS content
WHERE content.id IN (SELECT id FROM soft_deleted_content_ids);
