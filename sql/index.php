<?php

$baseTargetFilename = basename(strtolower($_GET['preview'] ?? ''), '.sql') . '.sql';

if (empty($baseTargetFilename)) {
    notFound();
}

$targetFile = __DIR__ . '/' . $baseTargetFilename;

if (! is_readable($targetFile)) {
    notFound();
}

header('Content-Type: text/plain; charset=UTF-8');
header("Content-Disposition: inline; filename={$baseTargetFilename}");

readfile($targetFile);

function notFound()
{
    http_response_code(404);
    echo "404 Not Found\n";
    exit;
}
