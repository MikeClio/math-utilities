CREATE TEMPORARY TABLE answer_content_ids 
SELECT content.id AS id
FROM clioonline_content_domain_model_content AS content
INNER JOIN clioonline_content_domain_model_content AS parent
	ON parent.id = content.originalcontent
INNER JOIN clioonline_content_domain_model_content_dimensions_join AS dimjoin
	ON dimjoin.content_content = content.id
INNER JOIN clioonline_content_domain_model_dimension AS dim
	ON dim.id = dimjoin.content_dimension AND dim.label = "answer";

DELETE conf
FROM clioonline_content_domain_model_contentconfiguration AS conf
WHERE conf.content IN (SELECT id FROM answer_content_ids);

DELETE prop
FROM clioonline_content_domain_model_contentcontenttypeproperty AS prop
WHERE prop.content IN (SELECT id FROM answer_content_ids);

DELETE dimjoin
FROM clioonline_content_domain_model_content_dimensions_join AS dimjoin
WHERE dimjoin.content_content IN (SELECT id FROM answer_content_ids);

DELETE content
FROM clioonline_content_domain_model_content AS content
WHERE content.id IN (SELECT id FROM answer_content_ids);
